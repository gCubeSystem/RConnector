package org.gcube.data.analysis.rconnector;

import java.io.File;
import java.io.FileWriter;

import javax.inject.Singleton;

import org.gcube.common.scope.api.ScopeProvider;

import lombok.extern.slf4j.Slf4j;


@Singleton
@Slf4j
public class ConfigFileWriter {


	public boolean write(Info info, String login, String fileName, String configFilePath, String scriptToExecute){
		if (!configFilePath.endsWith("/"))
			configFilePath=configFilePath+"/";
		File userDir = new File(configFilePath+login);
		if (!userDir.exists())
			userDir.mkdir();
		File configFile= new File(userDir, fileName);
		Utils.executeCommandLine(scriptToExecute,login, info.getToken(), ScopeProvider.instance.get());
		return  writeFile(info, configFile);
	}

	private boolean writeFile(Info info, File pathToFile){
		log.debug("writing config file");
		StringBuffer sb = new StringBuffer();
		sb.append(info.getTableName()+";");
		sb.append(info.getUserTableName()+";");

		sb.append(info.getFields());
		if (!info.getFields().endsWith(";"))
			sb.append(";");
		sb.append(info.getQueryColumns());
		if (!info.getQueryColumns().endsWith(";"))
			sb.append(";");
		sb.append(info.getDatabase().getDatabaseAddress()+";");
		sb.append(info.getDatabase().getDatabaseName()+";");
		sb.append(info.getDatabase().getDatabaseUsername()+";");
		sb.append(info.getDatabase().getDatabasePassword()+";");
		sb.append(info.getUsername()+";");
		sb.append(info.getToken()+"\n");
		
		try(FileWriter fw = new FileWriter(pathToFile,false)){
			fw.write(sb.toString());
		}catch(Exception e){
			log.error("error writing config file",e);
			return false;
		}
		log.debug("file written with line {}",sb.toString());
		return true;
	}
}
