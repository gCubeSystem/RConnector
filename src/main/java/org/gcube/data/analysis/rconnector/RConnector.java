package org.gcube.data.analysis.rconnector;

import javax.ws.rs.ApplicationPath;

import org.glassfish.jersey.server.ResourceConfig;

@ApplicationPath("/gcube/service/")
public class RConnector extends ResourceConfig  {

	public RConnector(){
		packages("org.gcube.data.analysis.rconnector");
	}

		
}
