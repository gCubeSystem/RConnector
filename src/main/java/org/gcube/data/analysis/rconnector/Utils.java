package org.gcube.data.analysis.rconnector;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Utils {

	private static Logger log = LoggerFactory.getLogger(Utils.class);
	
	protected static String executeCommandLine(String ... args){
		log.debug("executing command {}",Arrays.asList(args));
		Process process = null;
		String lastline = "";
		try {
			
			ProcessBuilder pb = new ProcessBuilder(args);
			process = pb.start();
			
			try {
				process.waitFor();
			} catch (InterruptedException e) {
				log.warn("interrupt Exception on process",e);
			}

			BufferedReader br = new BufferedReader(new InputStreamReader(process.getErrorStream()));
			String line = br.readLine();
			while (line!=null){
				lastline += line;
				line = br.readLine();
			}
		} catch (IOException e) {
			log.error("error executing command line",e);
		}
		log.debug("command result line is {} ",lastline);
		return lastline;
	}
	
}
