package org.gcube.data.analysis.rconnector;

import lombok.Data;

@Data
public class Info {

	private String username ="";
	private String token ="";
	private String tableName ="" ;
	private String userTableName ="" ;
	private String fields ="";
	private DataBaseInfo database = new DataBaseInfo();
	private String queryColumns= "";
	
	@Data
	public static class DataBaseInfo{
		private String databaseAddress="" ;
		private String databaseName="" ;
		private String databaseUsername ="" ;
		private String databasePassword ="" ;
	}
}
