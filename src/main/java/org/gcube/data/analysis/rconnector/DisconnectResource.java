package org.gcube.data.analysis.rconnector;

import javax.inject.Inject;
import javax.servlet.ServletContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import lombok.extern.slf4j.Slf4j;

@Path("disconnect")
@Slf4j
public class DisconnectResource {

			
	@Context ServletContext context;

	@Inject
	InfoRetriever infoRetriever;
	
	@GET
	@Produces(MediaType.TEXT_HTML)
	public Response disconnect(@Context HttpServletRequest req) {
		log.info("disconnect called ");
		String rStudioServerAddress = context==null?"":context.getInitParameter("rStudioAddress");
		String scriptToExecute = context.getInitParameter("unmountScript");
		
		String login = null;
		for (Cookie cookie : req.getCookies())
			if (cookie.getName().equals("user-id"))
				login = cookie.getValue();
		
		if (login==null)
			return Response.serverError().build();
		
		try{
			
			Utils.executeCommandLine(scriptToExecute, login);
			
			boolean found = false;
			for (Cookie cookie : req.getCookies())
				if (cookie.getName().equals("user-id")){ 
					found=true;
					break;
				}
			if (found)
				return Response.ok(context.getClassLoader().getResourceAsStream("logout.html"))
						.header(
								"Set-Cookie",
								String.format("user-id=deleted;Domain=.%s;Path=/;Expires=Thu, 01-Jan-1970 00:00:01 GMT",rStudioServerAddress)
								).build();
			else return Response.ok(context.getClassLoader().getResourceAsStream("inactivesession.html")).build();
		}catch(Exception e){
			log.error("erorr disconnecting from Rstudio",e);
			return Response.serverError().build();
		}
	}

}
