package org.gcube.data.analysis.rconnector;

import static org.gcube.data.analysis.tabulardata.clientlibrary.plugin.AbstractPlugin.history;
import static org.gcube.data.analysis.tabulardata.clientlibrary.plugin.AbstractPlugin.query;
import static org.gcube.data.analysis.tabulardata.clientlibrary.plugin.AbstractPlugin.tabularResource;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Inject;
import javax.inject.Singleton;

import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.database.DatabaseEndpointIdentifier;
import org.gcube.common.database.DatabaseProvider;
import org.gcube.common.database.endpoint.DatabaseEndpoint;
import org.gcube.data.analysis.rconnector.Info.DataBaseInfo;
import org.gcube.data.analysis.tabulardata.commons.webservice.types.TabularResource;
import org.gcube.data.analysis.tabulardata.model.column.Column;
import org.gcube.data.analysis.tabulardata.model.column.type.IdColumnType;
import org.gcube.data.analysis.tabulardata.model.datatype.GeometryType;
import org.gcube.data.analysis.tabulardata.model.metadata.common.NamesMetadata;
import org.gcube.data.analysis.tabulardata.model.metadata.table.DatasetViewTableMetadata;
import org.gcube.data.analysis.tabulardata.model.table.Table;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Singleton
public class InfoRetriever {
	
	@Inject
	DatabaseProvider databaseProvider;
	
	@SuppressWarnings("unchecked")
	public Info retrieve(String userName, Long tabularResourceId){
		try {
			TabularResource tr = tabularResource().build().getTabularResource(tabularResourceId);
			Table table = history().build().getLastTable(tabularResourceId);
			if (table.contains(DatasetViewTableMetadata.class))
				table= query().build().getTable(table.getMetadata(DatasetViewTableMetadata.class).getTargetDatasetViewTableId().getValue());

			Info info =new Info();
			info.setTableName(table.getName());
			StringBuilder queryOrder = new StringBuilder();;
			StringBuilder fields = new StringBuilder();
			for (Column col:table.getColumnsExceptTypes(IdColumnType.class)){
				fields.append(col.getMetadata(NamesMetadata.class).getTextWithLocale("en").getValue()).append(",");
				if (col.getDataType().getClass().equals(GeometryType.class))
					queryOrder.append("ST_AsText(").append(col.getName()).append(")").append(",");
				else queryOrder.append(col.getName()).append(",");
			}
			fields.deleteCharAt(fields.lastIndexOf(","));
			queryOrder.deleteCharAt(queryOrder.lastIndexOf(","));
			
			info.setFields(fields.toString());
			info.setQueryColumns(queryOrder.toString());
			info.setUsername(userName);
			info.setUserTableName(tr.getName());
			info.setToken(SecurityTokenProvider.instance.get());
			info.setDatabase(retrievedatabaseInfo());
			
			log.info("retrieved info are "+info);
			
			return info;
		} catch (Exception e) {
			log.error("error retrieving info ",e);
			throw new RuntimeException(e.getMessage());
		}
		
		
	}
	
	
	DataBaseInfo retrievedatabaseInfo(){
		DatabaseEndpointIdentifier endpointId = new DatabaseEndpointIdentifier("TabularData Database",
				"Data-ReadOnly");
		DatabaseEndpoint endpoint = databaseProvider.get(endpointId);	
		DataBaseInfo databaseInfo = new DataBaseInfo();
		databaseInfo.setDatabaseUsername(endpoint.getCredentials().getUsername());
		databaseInfo.setDatabasePassword(endpoint.getCredentials().getPassword());
		
		Pattern pattern = Pattern.compile("[^/]*//([^:]*)[^/]*/(.*)");
		Matcher m = pattern.matcher(endpoint.getConnectionString());
		m.find();
		String databaseAddress = m.group(1);
		String databaseName = m.group(2);
		
		databaseInfo.setDatabaseAddress(databaseAddress);
		databaseInfo.setDatabaseName(databaseName);
		return databaseInfo;
	}
	
}
